# Installation

1. Open your game page
2. Press on "Settings" in the top right and select "API Scripts"  
  ![Step 1](install_1.png)
3. Press on th e"Roll 20 API Script Library"-dropdown  
  ![Step 2](install_2.png)
4. Enter "Ht" in the  search, then select "HtmlBuilder"  
  ![Step 3](install_3.png)
5. Press the "Add Script"-button  
  ![Step 4](install_4.png)
6. Click "New Script" at the top  
  ![Step 5](install_5.png)
7. Paste everything from [this link](https://gitlab.com/azzurite/ElectiveActionOrder/raw/master/ElectiveActionOrder.js) into the big black box, enter an appropriate name, and hit the "Save Script"-button  
  ![Step 6](install_6.png)
8. After a few seconds, you should see "ElectiveActionOrder loaded" in the API output console at the bottom